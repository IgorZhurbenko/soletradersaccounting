﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SoleTraderBookkeepingApp.Data;

namespace SoleTraderBookkeepingApp.Migrations
{
    [DbContext(typeof(FinancialDataContext))]
    partial class FinancialDataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3");

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.Commodity", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<ulong>("TypeID")
                        .HasColumnType("INTEGER");

                    b.HasKey("ID");

                    b.ToTable("Commodities");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.CommodityType", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("CommodityTypes");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.Counterparty", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<ulong>("TypeID")
                        .HasColumnType("INTEGER");

                    b.HasKey("ID");

                    b.ToTable("Counterparties");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.CounterpartyType", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("CounterpartyTypes");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.Employee", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<ulong>("PositionID")
                        .HasColumnType("INTEGER");

                    b.HasKey("ID");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.Measure", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("Measures");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.MeasureUnit", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<float>("Coefficient")
                        .HasColumnType("REAL");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("MeasureUnits");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.OrderIncoming", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("CustomerID")
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<int>("ExpectedSettlementDateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("Moved")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("Number")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("Responsible")
                        .HasColumnType("INTEGER");

                    b.Property<int>("SettlementDateINT")
                        .HasColumnType("integer");

                    b.HasKey("ID");

                    b.ToTable("OrdersIncoming");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.OrderIncomingLineCommodities", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("CommodityID")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("MeasureUnitID")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("OwnerID")
                        .HasColumnType("INTEGER");

                    b.Property<float>("Price")
                        .HasColumnType("REAL");

                    b.Property<float>("Quantity")
                        .HasColumnType("REAL");

                    b.HasKey("ID");

                    b.ToTable("OrderIncomingLinesCommodities");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.OrderOutcoming", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<int>("ExpectedSettlementDateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("Moved")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("Number")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("Responsible")
                        .HasColumnType("INTEGER");

                    b.Property<int>("SettlementDateINT")
                        .HasColumnType("integer");

                    b.Property<ulong>("SupplierID")
                        .HasColumnType("INTEGER");

                    b.HasKey("ID");

                    b.ToTable("OrdersOutcoming");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.OrderOutcomingLineCommodities", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("CommodityID")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("MeasureUnitID")
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("OwnerID")
                        .HasColumnType("INTEGER");

                    b.Property<float>("Price")
                        .HasColumnType("REAL");

                    b.Property<float>("Quantity")
                        .HasColumnType("REAL");

                    b.HasKey("ID");

                    b.ToTable("OrderOutcomingLinesCommodities");
                });

            modelBuilder.Entity("SoleTraderBookkeepingApp.Models.Position", b =>
                {
                    b.Property<ulong>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("DateINT")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("MarkedForDeletion")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("Positions");
                });
#pragma warning restore 612, 618
        }
    }
}
