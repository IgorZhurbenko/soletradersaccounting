﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using SoleTraderBookkeepingApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Controllers
{
    public class MetadataDTO
    {
        public string Header { get; set; }
        public string ObjectName { get; set; }
        public List<Dictionary<string, string>> Fields { get; set; }

        public string[] TabularSections { get; set; }

        public int MetaID { get; set; }
        public virtual string SerializeForJS()
        {
            string QuoteSymbol = "\"";
            string result = "{" + QuoteSymbol + "Fields" + QuoteSymbol + " : " + "[";
            bool FirstColumn = true;
            bool FirstValue;

            foreach (var column in this.Fields)
            {
                if (!FirstColumn)
                {
                    result = result + ", {";
                }
                else { result = result + "{"; FirstColumn = false; }
                FirstValue = true;
                foreach (var KeyValue in column)
                {

                    string CommonPart = $"{QuoteSymbol}{KeyValue.Key}{QuoteSymbol}: {QuoteSymbol}{KeyValue.Value}{QuoteSymbol}";
                    if (!FirstValue)
                    { result = result + $", {CommonPart}"; }
                    else { result = result + CommonPart; FirstValue = false; }
                }
                result = result + '}';
            }

            result += "]}";

            return result;
        }

    }

    public class UniversalDTO_Index_AJAX : MetadataDTO
    {
        public UniversalDTO_Index_AJAX()
        { }
        public UniversalDTO_Index_AJAX(Type Model)
        {
            var ShowablesInfo = Model.GetProperties().Where(property => property.GetCustomAttribute(typeof(Showable)) != null &&
            property.GetCustomAttribute<Showable>().ShowInList)
                .OrderBy(elem => elem.GetCustomAttribute<Showable>().Order);

            List<Dictionary<string, string>> Fields = new List<Dictionary<string, string>>();

            foreach (PropertyInfo Showable in ShowablesInfo)
            {
                var ShowableAttr = Showable.GetCustomAttribute<Showable>();
                Fields.Add(
                        new Dictionary<string, string>()
                        {
                            { "Property", Showable.Name},
                            { "Representation", ShowableAttr.ShownName},
                            { "Showable", ShowableAttr != null? "True" : ""},
                            { "Filterable", Showable.GetCustomAttribute<NotFilterable>() == null? "True" : "" },
                            { "Type", Showable.PropertyType.Name},
                            { "ModelName", Showable.PropertyType == typeof(Reference)? ShowableAttr.ModelName : ""},
                            { "Editable", Showable.GetSetMethod() == null? "" : "true"}
                        }
                    );
            }

            this.ObjectName = Model.Name;
            this.Header = Model.GetCustomAttribute<MetadataAttribute>().PluralName;
            this.Fields = Fields;
            this.TabularSections = HasTabularSectionsAttribute.GetTabularSectionsNames(Model);
            this.MetaID = Model.GetCustomAttribute<MetadataAttribute>().MetaID;
        }

    }

    public class UniversalDTO_Edit_AJAX : MetadataDTO
    {
        public Reference ObjectReference { get; set; }
        public UniversalDTO_Edit_AJAX() { }
        public UniversalDTO_Edit_AJAX(Reference ObjectReference)
        {
            this.ObjectReference = ObjectReference;
            var ShowablesInfo = ObjectReference.ModelType.GetProperties().Where(property => property.GetCustomAttribute<Showable>() != null)
                .OrderBy(elem => elem.GetCustomAttribute<Showable>().Order);

            List<Dictionary<string, string>> Fields = new List<Dictionary<string, string>>();

            foreach (PropertyInfo Showable in ShowablesInfo)
            {
                Fields.Add(
                        new Dictionary<string, string>()
                        {
                            {"Property", Showable.Name},
                            {"Representation", Showable.GetCustomAttribute<Showable>().ShownName},
                            {"Showable", Showable.GetCustomAttribute<Showable>() != null? "True" : ""},
                            {"Type", Showable.PropertyType.Name},
                            {"Editable", Showable.GetSetMethod() != null? "True" : ""},
                            {"ModelName", Showable.GetCustomAttribute<Showable>().ModelName}
                        }
                    );
            }

            this.ObjectName = ObjectReference.ModelName;
            if (ObjectReference.ObjectID != 0) { this.Header = ObjectReference.GetObject(new Data.FinancialDataContext()).GetReferenceRepresentation(); }
            else { this.Header = "new " + ObjectName; }
            this.Fields = Fields;
            this.TabularSections = HasTabularSectionsAttribute.GetTabularSectionsNames(ObjectReference.ModelType);
        }
    }

    public static class UniversalRenderer
    {
        public static IActionResult UniversalIndex<Model>(this Controller controller)
        {
            return controller.View("UniversalIndex", new UniversalDTO_Index_AJAX()
            {
                Header = typeof(Model).GetCustomAttribute<MetadataAttribute>().PluralName,
                ObjectName = typeof(Model).Name,
                MetaID = typeof(Model).GetCustomAttribute<MetadataAttribute>().MetaID
            }
            ) ;
        }

        public static IActionResult UniversalEdit(this Controller controller, Reference ObjectReference)
        {
            return controller.View("UniversalEdit", new UniversalDTO_Edit_AJAX() { Header = "*new " + ObjectReference.ModelType.GetCustomAttribute<MetadataAttribute>().SingleName, ObjectName = ObjectReference.ModelType.Name, ObjectReference = ObjectReference});
        }

        public static IActionResult UniversalIndex(this Controller controller, Type Model)
        {
            return controller.View("UniversalIndex", new UniversalDTO_Index_AJAX()
            {
                Header = Model.GetCustomAttribute<MetadataAttribute>().PluralName,
                ObjectName = Model.Name,
                MetaID = Model.GetCustomAttribute<MetadataAttribute>().MetaID
            }
            );
        }

    }


}
