﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Controllers
{
    public static class ObjectsExtensions
    {
        public static bool FitsExternalFilter(this object obj, Dictionary<string, string> ExternalFilter)
        {
            Dictionary<PropertyInfo, string> ProcessedFilter = new Dictionary<PropertyInfo, string>();

            foreach (var KeyValue in ExternalFilter)
            {
                try { ProcessedFilter.Add(obj.GetType().GetProperty(KeyValue.Key), KeyValue.Value); }
                catch { return false; }
            }

            return FitsFilterAsString(obj, ProcessedFilter);
        }

        public static bool FitsFilterAsString(this object obj, Dictionary<PropertyInfo, string> Filter)
        {
            foreach (var Property in Filter)
            {
                try
                {
                    if (Property.Key.GetValue(obj).ToString() != Property.Value)
                    { return false; }
                }
                catch { return false; }
            }
            return true;
        }

        public static IEnumerable<object> TakeAtMost(this IEnumerable<object> obj, int n)
        {
            return obj.Take(Math.Min(obj.Count(), n));
        }
    }
}
