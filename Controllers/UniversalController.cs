﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using Newtonsoft.Json;
using SoleTraderBookkeepingApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;
using Microsoft.Extensions.ObjectPool;
using System.Reflection;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Reflection.Metadata;
using Microsoft.CodeAnalysis;
using System.Collections;
using Microsoft.AspNetCore.Authorization;

namespace SoleTraderBookkeepingApp.Controllers
{
    [Authorize]
    public class UniversalController : Controller
    {
        private readonly FinancialDataContext _context;

        public UniversalController(FinancialDataContext context)
        {
            _context = context;
        }

        public IActionResult Entity()
        {
            return this.UniversalEdit(Reference.TryParse(HttpContext.Request.QueryString.ToString().Split('=')[1]));
        }

        [HttpPost]
        public string RetrieveMetadata([FromBody] Dictionary<string, string> QueryContent)
        {
            switch (QueryContent["MetadataType"].ToLower())
            {
                case "list":
                    return System.Text.Json.JsonSerializer.Serialize(new UniversalDTO_Index_AJAX(Type.GetType("SoleTraderBookkeepingApp.Models." + QueryContent["ObjectName"])));
                case "element":
                    return System.Text.Json.JsonSerializer.Serialize(new UniversalDTO_Edit_AJAX(Reference.TryParse(QueryContent["ObjectReference"])));
                default:
                    return "{\"Status\": \nWrong request\"}";
            }
        }

        [HttpPost]
        public string RetrieveList([FromBody] Dictionary<string, object> QueryContent)
        {
            try
            {
                string ObjectName = QueryContent["ObjectName"].ToString();
                Type ThisType = Type.GetType(this.GetType().Namespace.Split('.')[0] + ".Models." + ObjectName);

                Dictionary<string, string> FilterStrings = JsonConvert.DeserializeObject<Dictionary<string, string>>(QueryContent["Filter"].ToString());
                //System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(QueryContent["Filter"]);

                var Context = typeof(FinancialDataContext).GetProperties()
                    .Where(prop => (prop.PropertyType.GenericTypeArguments.Count() > 0)
                        && (prop.PropertyType.GenericTypeArguments.First() == ThisType))
                    .First();

                if (FilterStrings.Count != 0)
                {
                    return System.Text.Json.JsonSerializer.Serialize(
                        (Context.GetValue(this._context) as IEnumerable<object>)
                   .Where(obj => obj.FitsExternalFilter(FilterStrings))
                   );
                }
                else
                {
                    var DataSet = Context.GetValue(this._context);
                    return JsonConvert.SerializeObject(DataSet as IEnumerable<object>);
                }

            }
            catch { return "{ \"Status\" : \"Error\""; }
        }

        [HttpPost]
        public string Retrieve([FromBody] Dictionary<string, string> queryContent)
        {
            try
            {
                Reference ObjectReference = new Reference(Convert.ToInt32(queryContent["ObjectReference"].Split('_')[0]),
                    Convert.ToUInt64(queryContent["ObjectReference"].Split('_')[1]));

                Entity obj = ObjectReference.GetObject(this._context);

                var TabularSections = new Dictionary<string, object[]>();

                var ObjectToSend = new { ObjectData = obj };
                string MessageToSend = JsonConvert.SerializeObject(ObjectToSend);
                return MessageToSend;

            }
            catch { return "{\"Status\":\"Error\"}"; }
        }

        [HttpPost]
        public string Record([FromBody] object queryContent)
        {
            string JSON = queryContent.ToString();
            var DeserializedJSON = System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, object>>(JSON);
            string JSONObjectData = DeserializedJSON["ObjectData"].ToString();

            try
            {
                Type ObjectType = Type.GetType("SoleTraderBookkeepingApp.Models." + (DeserializedJSON["ObjectType"].ToString()));

                MethodInfo ObjectDeserializingMethod = typeof(JsonConvert).GetMethod("DeserializeObject", new Type[] { typeof(string), typeof(Type) });
                var Object = ObjectDeserializingMethod.Invoke(null, new object[] { JSONObjectData, ObjectType });

                var Obj = Convert.ChangeType(Object, ObjectType) as Entity;

                var DeserializedTabularSections = JsonConvert.DeserializeObject<Dictionary<string, object>>(DeserializedJSON["TabularSections"].ToString());

                var NewTabularSections = new Dictionary<string, object[]>();

                if (Obj.ObjectReference.ObjectID == 0)
                {
                    if (Obj.Record(_context))
                    {
                        var Ref = (Convert.ChangeType(Object, ObjectType) as Entity).ObjectReference;

                        foreach (var TabularSection in DeserializedTabularSections)
                        {
                            var TableLinesDeserializingMethod = typeof(JsonConvert).GetMethod("DeserializeObject", 1, new Type[] { typeof(string) }).MakeGenericMethod(
                                new Type[] { typeof(List<>).MakeGenericType(Type.GetType("SoleTraderBookkeepingApp.Models." + TabularSection.Key)) });
                            var LinesArray = (TableLinesDeserializingMethod.Invoke(null, new object[] { TabularSection.Value.ToString() }) as IEnumerable<TableLine>);
                            foreach (var Line in LinesArray) { Line.OwnerID = Ref.ObjectID; }

                            var res = TableLine.RecordLinesArray(new FinancialDataContext(), Type.GetType("SoleTraderBookkeepingApp.Models." + TabularSection.Key), Obj.ID,
                            LinesArray);
                            if (res == null)
                            { return "{\"Status\": \"Error\"}"; }
                            else
                            {
                                NewTabularSections.Add(TabularSection.Key, res);
                            }
                        }

                        return JsonConvert.SerializeObject(new { ObjectData = Object, TabularSections = NewTabularSections });

                    }
                    else { { return "{\"Status\": \"Error\"}"; } }
                }
                else
                {
                    foreach (var TabularSection in DeserializedTabularSections)
                    {
                        var TableLinesDeserializingMethod = typeof(JsonConvert).GetMethod("DeserializeObject", 1, new Type[] { typeof(string) }).MakeGenericMethod(
                            new Type[] { typeof(List<>).MakeGenericType(Type.GetType("SoleTraderBookkeepingApp.Models." + TabularSection.Key)) });

                        var LinesArray = (TableLinesDeserializingMethod.Invoke(null, new object[] { TabularSection.Value.ToString() }) as IEnumerable<TableLine>);
                        var res = TableLine.RecordLinesArray(new FinancialDataContext(), Type.GetType("SoleTraderBookkeepingApp.Models." + TabularSection.Key), Obj.ID,
                        LinesArray);
                        if (res == null)
                        { return "{\"Status\": \"Error\"}"; }
                        else
                        {
                            NewTabularSections.Add(TabularSection.Key, res);
                        }
                    }

                    if (Obj.Record(_context))
                    {
                        return JsonConvert.SerializeObject(new { ObjectData = Object, TabularSections = NewTabularSections });
                    }
                    else
                    {
                        return "{\"Status\": \"Error\"}";
                    }
                }
            }
            catch
            {
                return "{\"Status\": \"Error\"}";
            }
        }

        public string GetReferables([FromBody] Dictionary<string, string> queryContent)
        {
            Type objectType = Type.GetType("SoleTraderBookkeepingApp.Models." + queryContent["ObjectType"]);
            string Filter = queryContent.ContainsKey("Filter") ? queryContent["Filter"] : "";

            var References = new List<Reference>();

            var ReferableObjects = (typeof(FinancialDataContext).GetProperties().Where(prop => prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericArguments().First() == objectType)
                .First().GetValue(_context) as IEnumerable<Entity>);

            if (ReferableObjects.Count() < 1) { return JsonConvert.SerializeObject(new object[0]); }

            else
            {
                return JsonConvert.SerializeObject(
                ReferableObjects.Select(obj => obj.ObjectReference).Where(Ref => Filter == "" || Ref.Representation.ToLower().Contains(Filter.ToLower())).OrderBy(elem => elem.Representation)
            );
            }
        }

        public IActionResult Index()
        {
            int MetaID;
            try
            {
                MetaID = Convert.ToInt32(HttpContext.Request.QueryString.ToString().Split('=')[1]);
            }
            catch
            {
                return NotFound();
            }

            Type type = MetadataAttribute.GetTypeByMetaID(MetaID);

            if (type != null)
            {
                return this.UniversalIndex(type);
            }
            else { return NotFound(); }
        }
    }
}
