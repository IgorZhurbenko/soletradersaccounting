﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SoleTraderBookkeepingApp.Data;
using SoleTraderBookkeepingApp.Models;


namespace SoleTraderBookkeepingApp.Controllers
{
    public class JSONMessage
    {
        public string Text { get; set; }
    }
    [AllowAnonymous]
    public class CommodityTypesController : Controller
    {
        private readonly FinancialDataContext _context;

        public CommodityTypesController(FinancialDataContext context)
        {
            _context = context;
        }

        // GET: CommodityTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.CommodityTypes.ToListAsync());
        }

        // GET: CommodityTypes/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commodityType = await _context.CommodityTypes
                .FirstOrDefaultAsync(m => m.ID == id);
            if (commodityType == null)
            {
                return NotFound();
            }

            return View(commodityType);
        }

        // GET: CommodityTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CommodityTypes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,ID,Date,MarkedForDeletion")] CommodityType commodityType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(commodityType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(commodityType);
        }

        // GET: CommodityTypes/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commodityType = await _context.CommodityTypes.FindAsync(id);
            if (commodityType == null)
            {
                return NotFound();
            }
            return View(commodityType);
        }

        // POST: CommodityTypes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("Name,ID,DateINT,MarkedForDeletion")] CommodityType commodityType)
        {
            if (id != commodityType.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(commodityType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommodityTypeExists(commodityType.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(commodityType);
        }

        // GET: CommodityTypes/Delete/5
        //public async Task<IActionResult> Delete(ulong? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var commodityType = await _context.CommodityType
        //        .FirstOrDefaultAsync(m => m.ID == id);
        //    if (commodityType == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(commodityType);
        //}

       

        [HttpPost]       
        public string PostData([FromBody] Dictionary<string, string> Text)
        {
            //string QueryText = new StreamReader(Request.Body).ReadToEndAsync().Result;
            //var JSON = JsonSerializer.Deserialize<Dictionary<string, string[]>>(IncomingJSON.Text);
            Thread.Sleep(5000);
            return Text["Message"];
        }
        private string GetRequestContentAsString()
        {
            return Request.Query.ToString();            
        }

        // POST: CommodityTypes/Delete/5
        [HttpPost
            //, ActionName("Delete")
            ]
        //[ValidateAntiForgeryToken]
        public async Task<string> Delete()
        {
            string IncomingJSON = GetRequestContentAsString();
            try
            {
                int[] IDsToDelete = JsonSerializer.Deserialize<Dictionary<string, int[]>>(IncomingJSON)["IDsToDelete"];
                var commodityTypes = await _context.CommodityTypes.FindAsync(IDsToDelete);
                _context.CommodityTypes.RemoveRange(commodityTypes);
                await _context.SaveChangesAsync();
                return "200 OK";
            }
            catch { return "500"; }
        }

        private bool CommodityTypeExists(ulong id)
        {
            return _context.CommodityTypes.Any(e => e.ID == id);
        }
    }
}
