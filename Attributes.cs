﻿using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    public enum DateVariant { Date, DateTime, Time }

    public class HasTabularSectionsAttribute : Attribute
    {
        public string[] AttachedTabularSectionsLinesTypesNames;
        public HasTabularSectionsAttribute(params string[] AttachedTabularSectionsLinesTypesNames)
        {
            this.AttachedTabularSectionsLinesTypesNames = AttachedTabularSectionsLinesTypesNames;
        }

        public HasTabularSectionsAttribute(params Type[] AttachedTabularSectionsLinesTypes)
        {
            this.AttachedTabularSectionsLinesTypesNames = AttachedTabularSectionsLinesTypes.Select(type => type.Name).ToArray();
        }

        public static string[] GetTabularSectionsNames(Type Model)
        {
            var attr = Model.GetCustomAttribute<HasTabularSectionsAttribute>();
            if (attr == null) { return new string[0]; }
            else
            {
                return Model.GetCustomAttribute<HasTabularSectionsAttribute>().AttachedTabularSectionsLinesTypesNames;
            }
        }
    }

    public class NotFilterable : Attribute
    {

    }

    public class Showable : Attribute
    {
        public string ShownName;
        public short Order;
        public bool ShowInList;
        public string ModelName;
        public Showable(string Name,  string ModelName, short Order = 1000, bool ShowInList = true) { this.ShownName = Name; this.Order = Order; this.ShowInList = ShowInList; this.ModelName = ModelName; }
    }

    public class NotEditableField : Attribute
    {

    }

    public class MetadataAttribute : Attribute
    {
        public int MetaID
        {
            get;
            set;
        }
        public string SingleName;
        public string PluralName;



        public MetadataAttribute(int MetaID, String SingleName, string PluralName)
        {
            this.MetaID = MetaID;
            this.SingleName = SingleName;
            this.PluralName = PluralName;
        }

        public static Type GetTypeByMetaID(int MetaID)
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(type => type.GetCustomAttribute<MetadataAttribute>() != null
            && type.GetCustomAttribute<MetadataAttribute>().MetaID == MetaID).FirstOrDefault();
        }

        private class NonUniqueMetaID : Exception
        {

        }


    }

    public class Date : Attribute
    {
        public DateVariant DateVariant { get; set; }
        public Date(DateVariant dateVariant) { this.DateVariant = dateVariant; }
    }

    public class Navigatable : Attribute
    {
        bool Universal = true;

        public static Type[] GetAllNavigatables()
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(prop => prop.GetCustomAttribute<Navigatable>() != null).ToArray();
        }

        public static MetadataAttribute[] GetMetadataAttributesOfAllNavigatables()
        {
            return GetAllNavigatables().Select(elem => elem.GetCustomAttribute<MetadataAttribute>()).ToArray();
        }
    }
}
