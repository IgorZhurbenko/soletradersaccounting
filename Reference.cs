﻿using Microsoft.CodeAnalysis.CSharp.Syntax;

using SoleTraderBookkeepingApp.Data;
using SoleTraderBookkeepingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
//using System.Reflection.Metadata;

namespace SoleTraderBookkeepingApp
{
    public class Reference
    {
        public int MetaID { get; set; }

        public UInt64 ObjectID { get; set; }

        public string Representation { get; set; }

        [JsonIgnore]
        public Type ModelType
        {
            get
            {
                if (this.MetaID == 0) { return null; }
                return MetadataAttribute.GetTypeByMetaID(MetaID);
            }
        }

        public string ModelName { get; set; }

        public static Reference TryParse(string ReferenceString)
        {

            try
            {
                return new Reference(Convert.ToInt32(ReferenceString.Split('_')[0]), Convert.ToUInt64(ReferenceString.Split('_')[1]));
            }
            catch { return null; }
        }

        public Reference(int MetaID, UInt64 ObjectID, string Representation = "")
        {
            this.MetaID = MetaID; this.ObjectID = ObjectID; this.Representation = Representation; this.ModelName = "";
        }

        public Reference(string ModelName, UInt64 ObjectID, string Representation)
        {
            this.ModelName = ModelName; this.ObjectID = ObjectID; this.Representation = Representation;
        }

        public Entity GetObject(FinancialDataContext context)
        {
            if (this.MetaID == 0 || this.ObjectID == 0) { return null; }
            Entity res;
            try
            {
                res = (Entity)context.Find(this.ModelType, (ulong)this.ObjectID);
            }
            catch { res = null; }

            return res;
        }

        public override string ToString()
        {
            return this.MetaID + "_" + this.ObjectID;
        }

        public static implicit operator Reference(string obj)
        {
            try { return new Reference(Convert.ToInt32(obj.Split('_')[0]), Convert.ToUInt64(obj.Split('_')[1])); } catch { return null; }
        }

        public static implicit operator Reference<Entity>(Reference Ref)
        {
            Type GenericType = typeof(Reference).MakeGenericType(Ref.ModelType);
            return (Reference<Entity>)GenericType.GetConstructor(new Type[] { typeof(ulong), typeof(string) }).Invoke(new object[] { Ref.ObjectID });
        }
        
        
    }

    

    public class Reference<ReferredType> where ReferredType : Entity
    {
        ulong ObjectID { get; set; }

        int MetaID { get { return typeof(ReferredType).GetCustomAttribute<MetadataAttribute>().MetaID; } }
        string Representation { get; set; }

        public Reference(ulong ID, string Representation = "")
        {
            this.ObjectID = ID;
            this.Representation = Representation;
        }

        public static implicit operator Reference(Reference<ReferredType> reference)
        {
            return new Reference(reference.MetaID, reference.ObjectID, reference.Representation);
        }


    }
}
