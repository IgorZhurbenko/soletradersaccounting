﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Microsoft.EntityFrameworkCore;
using SoleTraderBookkeepingApp.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using SoleTraderBookkeepingApp.Models;

namespace SoleTraderBookkeepingApp.Data
{
    public class FinancialDataContext: DbContext
    {
        

        private readonly HttpContext httpContext;
        public FinancialDataContext()
        { }

        public FinancialDataContext(DbContextOptions<FinancialDataContext> options, IHttpContextAccessor httpContextAccessor = null)
            :base(options)
        {
            httpContext = httpContextAccessor?.HttpContext;
            //this.Database.EnsureCreated();
        }

        public FinancialDataContext(ref DbContextOptionsBuilder DbOptions, ref HttpContext context)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder DbOptions)
        {
            //this.Database.EnsureCreated();
            
            //if ((DbOptions != null) || (!DbOptions.IsConfigured) || (this.httpContext != null))
            //{
            //    var clientClaim = httpContext?.User.Claims.Where(c => c.Type == ClaimTypes.GroupSid).Select(c => c.Value).SingleOrDefault();
            //    if (clientClaim == null) clientClaim = "DEBUG"; // Let's say there is no http context, like when you update-database from PMC
            //    DbOptions.UseSqlite(connectionString: "FileName = ./Databases/" + httpContext.User.Identity.Name.Replace("@","") + ".db3");
            //}
            string DBName="DB";

            //if ((httpContext != null) || (httpContext.User != null))
            //{ DBName = this.httpContext.User.Identity.Name.Replace("@", ""); }
           

            DbOptions.UseSqlite(connectionString: "FileName=./Data/" + DBName + ".db3");
        }

        public DbSet<Commodity> Commodities { get; set; }

        public DbSet<SoleTraderBookkeepingApp.Models.CommodityType> CommodityTypes { get; set; }

        public DbSet<SoleTraderBookkeepingApp.Models.OrderIncoming> OrdersIncoming { get; set; }

        public DbSet<SoleTraderBookkeepingApp.Models.Measure> Measures { get; set; }

        public DbSet<SoleTraderBookkeepingApp.Models.MeasureUnit> MeasureUnits { get; set; }

        public DbSet<SoleTraderBookkeepingApp.Models.OrderIncomingLineCommodities> OrderIncomingLinesCommodities { get; set; }
        public DbSet<SoleTraderBookkeepingApp.Models.OrderOutcoming> OrdersOutcoming{ get; set; }
        public DbSet<SoleTraderBookkeepingApp.Models.OrderOutcomingLineCommodities> OrderOutcomingLinesCommodities { get; set; }
        public DbSet<SoleTraderBookkeepingApp.Models.Counterparty> Counterparties { get; set; }
        public DbSet<SoleTraderBookkeepingApp.Models.CounterpartyType> CounterpartyTypes { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Position> Positions { get; set; }
    }
}
