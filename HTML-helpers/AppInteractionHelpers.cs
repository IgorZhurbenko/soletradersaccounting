﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.IO;

namespace Microsoft.AspNetCore.Mvc.Rendering
{
    
    public static class AppConfigurationHelpers
    {
        public static string BaseIP;
        static AppConfigurationHelpers()
        {
            BaseIP = File.ReadAllText(@"wwwroot\IP.txt");           
        }
        public static string GetAppAddress(this IHtmlHelper helper)
        {
            //return @"https://" + System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.Where(ip => ip.AddressFamily.ToString() == "InterNetwork").First().ToString() + ":44328";
            //return "https://localhost:44328";
            return BaseIP; 
        }

        public static string FilterFor<Model>(Dictionary<string, Dictionary<string,string>[]> PropertiesNamesValuePairs)
        {
            string result = "<table id='filter'>";
            Type type = typeof(Model);
            MemberInfo[] Properties = type.GetProperties();
            int i = 0;
            foreach (MemberInfo Property in Properties)
            {
                i++;

                result +=  $"\n<tr id='FilterRow{i}'>\n " +
                    $"<td> {Property.Name} </td>\n" +
                    $"<td> ";
            }
            return result;
        }
    }

    public static class AjaxRequests
    {
        public static string SendLengthyRequest(this IHtmlHelper helper, 
            string RequestType, string ControllerName, 
            string ControllerMethod, string Body)
        {
            return "<>";
        }

        
    }

    
}
