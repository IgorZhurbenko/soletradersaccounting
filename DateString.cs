﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp
{
    public class DateString
    {
        public const int InitialYear = 1980;
        private int Value { get; set; }

        [JsonIgnore]
        public string Representation { get { return this; }  }

        public string ORepresentation { get { return ((DateTime)this).ToString("yyyy-MM-ddThh:mm"); } }

        private DateTime ToDateTime()
        {
            return DateTime.MinValue.AddYears(InitialYear).AddSeconds(this.Value);
        }

        public static implicit operator string(DateString obj)
        {
            return obj.ToDateTime().ToString();
        }

        public static implicit operator DateTime(DateString obj)
        {
            return obj.ToDateTime();
        }

        public static implicit operator ulong(DateString obj)
        {
            return (ulong)obj.Value;
        }

        public static implicit operator int(DateString obj)
        {
            return obj == null? 0 : obj.Value;
        }

        public static implicit operator Int64(DateString obj)
        {
            return obj;
        }

        public static implicit operator DateString(int obj)
        {
            return new DateString() { Value = obj };
        }

        public static implicit operator DateString(DateTime obj)
        {
            return new DateString() { Value = (int)obj.AddYears(-DateString.InitialYear).Subtract(DateTime.MinValue).TotalSeconds };
        }

        public static implicit operator DateString(string obj)
        {
            try
            {
                try 
                {
                    
                    return DateTime.ParseExact(obj, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch 
                {                     
                    return DateTime.Parse(obj);
                }
            }
            catch { return Convert.ToInt32(obj); }
        }

        public static implicit operator DateString(ulong obj)
        {
            return (int)obj;
        }

        public static implicit operator DateString(long obj)
        {
            return (int)obj;
        }

    }

    

}

