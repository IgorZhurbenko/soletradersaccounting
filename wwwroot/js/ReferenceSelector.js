﻿class ReferenceSelector {
    constructor(PropertyName, ObjectType, Adress, AppendTo) {
        var TextBox = document.createElement("input");
        TextBox.id = PropertyName;
        TextBox.type = "text";
        var Selector = document.createElement("select");
        Selector.visible = false;
        Selector.onselect = function() {
            TextBox.value = this.innerHTML;
            TextBox.ReferenceValue = this.value;
            this.visible = false;
        };
        TextBox.onchange = function() {
            Selector.visible = true;
            GetReferables(ObjectType, Adress, this.value, function(ResponseText) {
                var References = JSON.parse(ResponseText);
                ClearDropdown(Selector);
                for (var Reference of References) {
                    var NewOption = document.createElement("option");
                    NewOption.value = Reference.MetaID + "_" + Reference.ObjectID;
                    NewOption.innerHTML = Reference.Representation;
                    Selector.appendChild(NewOption);
                }
            });
            dropDown(Selector);
        };
        AppendTo.appendChild(TextBox);
        AppendTo.appendChild(Selector);
    }
}
