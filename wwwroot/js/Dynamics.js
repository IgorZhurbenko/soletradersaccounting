﻿function RemoveObjectFromArray(Array, ObjectToRemove) {
    for (var i in Array) {
        if (Array[i] === ObjectToRemove) var Delete = true; break;
    }
    if (Delete) Array.splice(i, 1);
}

function GetMetadata(ObjectName, Address, MetadataType, ActionOnFinish, ObjectReference = "") {
    var Request = new XMLHttpRequest();
    Request.open("POST", Address, true);
    Request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
    var SentData = { 'ObjectName': ObjectName, 'MetadataType': MetadataType, "ObjectReference": ObjectReference }
    var json = JSON.stringify(SentData)
    //console.log("Sending data to " + Address)
    Request.send(json);
    Request.onreadystatechange = function () {
        if (this.readyState == 4) {
            ActionOnFinish(this.responseText);
        }
    }
}

function GetReferables(ObjectType, Address, Filter, ActionOnFinish) {
    var Request = new XMLHttpRequest();
    Request.open("POST", Address, true);
    Request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
    var SentData = { 'ObjectType': ObjectType, "Filter": Filter }
    var json = JSON.stringify(SentData)
    Request.send(json);
    Request.onreadystatechange = function () {
        if (this.readyState == 4) {
            ActionOnFinish(this.responseText);
        }
    }
}

function ClearDropdown(DropDown) {
    var select = DropDown;
    var length = select.options.length;
    for (i = length - 1; i >= 0; i--) {
        select.options[i] = null;
    }
}




class ReferenceSelector {
    set Value(value) {
        if (value) {
            this.Elem.value = value.Representation == "0_0" || !value.Representation ? "None" : value.Representation;
            this.Elem.ReferenceValue = value.MetaID + "_" + value.ObjectID;
        }
        else {
            this.Elem.value = "None";
            this.Elem.ReferenceValue = "0_0";
        }
    }

    get Value() {
        return {
            "MetaID": this.Elem.ReferenceValue.split('_')[0], "ObjectID": this.Elem.ReferenceValue.split('_')[1], "Representation": this.Elem.value
        }
    }

    constructor(id, PropertyName, ObjectType, BaseAddress, AppendTo, InitialReference = { "MetaID": "0", "ObjectID": "0", "Representation": "None" }) {
        var TextBox = document.createElement("input");
        this.Elem = TextBox;
        this.Value = InitialReference;
        this.Property = PropertyName;
        this.ObjectType = ObjectType;
        TextBox.id = id;
        TextBox.type = "text";
        this.TextBox = TextBox;
        this.Selector = Selector;
        var obj = this;
        var Selector = document.createElement("select");
        Selector.hidden = true;
        var obj = this;
        Selector.onselect = function () {
            TextBox.value = this.innerHTML;
            TextBox.ReferenceValue = this.value;
            this.hidden = true;
        };

        TextBox.oninput = function () {
            Selector.hidden = false;
            GetReferables(ObjectType, BaseAddress + "/Universal/GetReferables", obj.Value.Representation, function (ResponseText) {
                var References = JSON.parse(ResponseText);
                ClearDropdown(Selector);

                for (var Reference of References) {
                    var NewOption = document.createElement("option");
                    NewOption.value = Reference.MetaID + "_" + Reference.ObjectID;
                    NewOption.innerHTML = Reference.Representation;
                    Selector.appendChild(NewOption);
                }

                var NewOption = document.createElement("option");
                NewOption.value = "0" + "_" + "0";
                NewOption.innerHTML = "None";
                Selector.appendChild(NewOption);

            });
        };

        TextBox.onblur = function () {
            if (document.activeElement != Selector) {
                Selector.selectedIndex = 0;
                TextBox.ReferenceValue = Selector.options[0].value;
                Selector.hidden = true;
                obj.Value = { "MetaID": Selector.options[0].value.split('_')[0], "ObjectID": Selector.options[0].value.split('_')[1], "Representation": Selector.options[0].innerHTML }
            }
        }

        TextBox.onchange = TextBox.onblur;
        AppendTo.appendChild(TextBox);
        AppendTo.appendChild(Selector);
    }
}

class ValueContainer {
    set Value(value) {
        if (value != undefined && value != null) {
            if (this.Editable) {
                if (this.ValueType == "Reference") {
                    this.ReferenceSelector.Value = value;
                }
                else if (this.ValueType == "DateString") {
                    this.Elem.value = value.ORepresentation;
                }
                else {
                    this.Elem.value = value;
                }
            }
            else {
                if (this.ValueType == "Reference") {
                    if (value.MetaID == 0 || !this.Interactible) {
                        this.Elem.removeAttribute("href");
                        this.Elem.disabled = true;
                        this.Elem.innerHTML = value.Representation == "0_0" ? "None" : value.Representation;
                        this.Elem.StoredReference = value;
                    }
                    else {
                        this.Elem.setAttribute("href", this.BaseAddress + "/Universal/Entity/?ref=" + value.MetaID + "_" + value.ObjectID);
                        this.Elem.innerHTML = value.Representation;
                        this.Elem.disabled = false;
                    }
                }
                else if (this.ValueType == "DateString") {
                    this.Elem.innerHTML = value.ORepresentation.replace("T", " ").split("-").join(".");
                }
                else {
                    this.Elem.value = value;
                    this.Elem.innerHTML = value;
                }
            }
        }
    }

    get Value() {
        if (this.Editable) {
            if (this.ValueType == "Reference") {
                return this.ReferenceSelector.Value;
            }
            else {
                return this.Elem.value;
            }
        }
        else {
            if (this.ValueType == "Reference") {
                if (this.Elem.href != "") {
                    var ref = this.Elem.href.split('=')[1];
                    return {
                        "MetaID": ref.split('_')[0], "ObjectID": ref.split('_')[1], "Representation": this.Elem.innerHTML
                    }
                }
                else { return this.Elem.StoredReference; }
            }
            else {
                return this.Elem.value;
            }
        }
    }

    CheckValue()
    {
        if (this.ValueType == "UInt64" || this.ValueType == "UInt32" || this.ValueType == "Int64"
            || this.ValueType == "Int32" || this.ValueType  == "Single" || this.ValueType  == "Double") {
            return String(this.Value) == String(Number(this.Value));
        }
        else return true;
    }

    constructor(id, Property, ValueType, InitialValue, ModelName, Editable, BaseAddress, AppendTo, Prepend = false, Interactible = true) {
        this.Editable = Editable;
        this.ModelName = ModelName;
        this.ValueType = ValueType;
        this.Property = Property;
        this.BaseAddress = BaseAddress;
        this.Interactible = Interactible;
        var obj = this;
        if (!Editable) {
            if (ValueType == "Reference") {
                this.Elem = document.createElement("a");
                this.Value = InitialValue;
            }

            else {
                this.Elem = document.createElement("p");
                this.Value = InitialValue;
            }
        }
        else {
            if (ValueType == "Reference") {
                this.ReferenceSelector = new ReferenceSelector(id, Property,
                    ModelName,
                    BaseAddress, AppendTo, InitialValue);
                this.Elem = this.ReferenceSelector.Elem;
                this.Value = InitialValue;
            }
            else if (ValueType == "DateString") {
                this.Elem = document.createElement("input");
                this.Elem.setAttribute("type", "Datetime-local");
                this.Elem.setAttribute("min", "1990-01-02T00:00");
                this.Elem.setAttribute("max", "2100-12-31T00:00");
                this.Elem.id = id;
                this.Value = InitialValue;
            }
            else if (ValueType == "Boolean") {
                this.Elem = document.createElement("input");
                this.ELem.type = "checkbox";
                this.Elem.id = id;
                this.Value = InitialValue;

            }
            else {
                this.Elem = document.createElement("input");
                this.Elem.type = "text";
                this.Elem.id = id;
                this.Value = InitialValue;
                this.Elem.oninput = function() {
                    if (!obj.CheckValue()) {
                        this.setAttribute("class", "TextBoxBottomLineError");
                    }
                    else { this.setAttribute("class", "TextBoxBottomLine"); }
                }

            }
        }
        if (!Prepend) {
            AppendTo.appendChild(this.Elem);
        }
        else { AppendTo.prepend(this.Elem); }
        this.Elem.setAttribute("class", "TextBoxBottomLine");
    }
}

class TableRow {

    get Value() {
        var obj = {};
        for (var Column of this.Columns) {
            obj[Column.Property] = this[Column.Property].Value;
        }
        return obj;
    }

    set Value(value) {
        for (var Column of this.Columns) {
            if (value[Column.Property] != undefined) {
                this[Column.Property].Value = value[Column.Property];
            }
        }
    }

    Delete() {
        this.Row.remove();
        RemoveObjectFromArray(this.Owner.Rows, this);
    }

    constructor(id, Columns, InitialData, AppendTo, Editable, BaseAddress, Owner) {
        var obj = this;
        this.Owner = Owner;
        var nrow = AppendTo.childNodes.length + 1
        var NewRow = document.createElement("tr");
        this.Row = NewRow;
        NewRow.id = id;
        this.Columns = Columns;

        if (Editable) {
            var DeleteColumn = document.createElement("td");
            var DeleteButton = document.createElement("button");
            DeleteButton.setAttribute("class", "NativeButton");
            DeleteColumn.appendChild(DeleteButton);
            var DeleteImage = document.getElementById("DeleteImage").cloneNode();
            DeleteImage.hidden = false;
            DeleteButton.appendChild(DeleteImage);
            DeleteButton.onclick = function () {
                obj.Delete();
            }
            NewRow.appendChild(DeleteColumn);
        }

        for (var Column of Columns) {
            var NewColumn = document.createElement("td");
            this[Column.Property] = new ValueContainer(id + Column.Property, Column.Property, Column.Type, InitialData[Column.Property], Column.ModelName,
                Editable && Column.Editable, BaseAddress, NewColumn);
            NewRow.appendChild(NewColumn);
        }

        AppendTo.appendChild(NewRow);

    };

    Assemble() {
        var obj = {};
        for (var Column of this.Columns) {
            if (Column.Type != "Reference") {
                obj[Column.Property] = this[Column.Property].Value;
            }
            else {
                var val = this[Column.Property].Value;
                obj[Column.Property] = val.MetaID + "_" + val.ObjectID;
            }
        }
        return obj;
    }
}

class DynamicTable {

    get Value() {
        var obj = [];
        for (var Row of this.Rows) {
            obj.push(Row.Value);
        }
        return obj;
    }

    set Value(value) {
        if (value.length > this.Rows.length) {
            for (var i in this.Rows) {
                this.Rows[i].Value = value[i];
            }
            for (var i = 0; i < value.length; i++) {
                this.Rows.push(new TableRow("", this.Columns, value[i], this.ElementsTable.Body, this.Editable, this.BaseAddress, this));
            }
        }
        if (value.length == this.Rows.length) {
            for (var i in this.Rows) {
                this.Rows[i].Value = value[i];
            }
        }

        if (value.length < this.Rows.length) {
            for (var i in value) {
                this.Rows[i].Value = value[i];
            }
            var n = this.Rows.length;
            for (i; i < n; i++) {
                this.Rows[i].Delete();
                this.Rows.pop();
            }
        }

    }

    constructor(id, columnsGivenByServer, BaseAddress, ObjectName, MetaID, AppendTo, Owner, InitialFilter = {}, Editable = false) {
        this.Owner = Owner;
        this.ObjectName = ObjectName;
        this.BaseAddress = BaseAddress;
        this.InitialFilter = InitialFilter;
        this.MetaID = MetaID;
        this.Editable = Boolean(Editable);
        this.Rows = [];
        var ReplacedQuotesString = columnsGivenByServer.replace(/&quot;/g, '\"');
        this.Columns = JSON.parse(ReplacedQuotesString).Fields;
        this.WholeElement = document.createElement("div");
        this.WholeElement.id = id;
        this.FilterTable = document.createElement("table");
        this.ElementsTable = document.createElement("table");
        this.ElementsTable.Body = document.createElement("tbody");
        this.FilterTable.setAttribute("class", "table");
        this.ElementsTable.setAttribute("class", "table");
        this.FilterTable.Body = document.createElement("tbody");
        this.ElementsTable.Head = document.createElement('thead');
        this.ElementsTable.HeadRow = document.createElement("tr");
        this.ElementsTable.HeadRow.style.fontWeight = 'bold';
        this.AddButton = document.createElement("button");
        var obj = this;
        if (this.Editable) {
            this.AddButton.onclick = function () {
                obj.Rows.push(new TableRow("", obj.Columns, "", obj.ElementsTable.Body, true, obj.BaseAddress));
            }
        } else {
            this.AddButton.onclick = function () {
                document.location.href = "" + obj.BaseAddress + "/universal/Entity/?ref=" + obj.MetaID + "_0";
            }
        }
        var AddImage = document.getElementById("AddImage").cloneNode(false);
        AddImage.hidden = false;
        this.AddButton.setAttribute("class", "NativeButton");
        this.AddButton.appendChild(AddImage);
        this.WholeElement.appendChild(this.AddButton);
        this.ShowFiltersButton = document.createElement("button");
        this.ShowFiltersButton.setAttribute("class", "NativeButton");
        this.FilterTable.hidden = true;
        var filterimage = document.getElementById("FilterImage").cloneNode();
        filterimage.hidden = false;
        this.ShowFiltersButton.appendChild(filterimage);

        var obj = this;
        this.ShowFiltersButton.onclick = function () {
            obj.FilterTable.hidden = !obj.FilterTable.hidden;
            //obj.ShowFiltersButton.innerHTML = FilterTable.hidden ? "Show filters" : "Hide filters"
        }
        this.WholeElement.appendChild(this.ShowFiltersButton);

        if (this.Editable) { this.ElementsTable.HeadRow.appendChild(document.createElement("td")); }

        for (var column of this.Columns) {
            if (column.Filterable) {
                var NewRow = document.createElement('tr');
                var NewColumn = document.createElement('td');
                NewColumn.innerHTML = column.Representation;
                NewColumn.PropertyName = column.Property;
                NewRow.appendChild(NewColumn)
                var NewColumn = document.createElement('td');
                var TextBox = document.createElement('input');
                TextBox.type = 'text';
                NewColumn.appendChild(TextBox);
                NewRow.appendChild(NewColumn);
                this.FilterTable.Body.appendChild(NewRow);
            }
            if (column.Showable) {
                var NewColumn = document.createElement('td');
                NewColumn.innerHTML = column.Representation;
                NewColumn.PropertyName = column.Property;
                this.ElementsTable.HeadRow.appendChild(NewColumn);
            }
        }
        this.FilterTable.appendChild(this.FilterTable.Body);
        this.WholeElement.appendChild(this.FilterTable);
        this.ElementsTable.Head.appendChild(this.ElementsTable.HeadRow);
        this.ElementsTable.appendChild(this.ElementsTable.Head);

        this.ElementsTable.appendChild(this.ElementsTable.Body);
        this.WholeElement.appendChild(this.ElementsTable);
        AppendTo.appendChild(this.WholeElement);
        this.GetContents();
    }

    ClearContents() {
        this.Value = [];
    }

    FillInContents() {
    }

    GatherFilters() {
        return this.InitialFilter;
    }

    GetContents() {
        var Request = new XMLHttpRequest();
        var Address = this.BaseAddress + "/" + "universal" + "/" + "retrievelist"
        Request.open("POST", Address, true);
        Request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
        var SentData = { 'ObjectName': this.ObjectName, "Filter": this.GatherFilters() }
        var json = JSON.stringify(SentData)
        console.log("Sending data to " + Address)
        Request.send(json);
        var obj = this;
        Request.onreadystatechange = function () {
            if (this.readyState == 4) {
                obj.Value = JSON.parse(Request.responseText);
            }
        }
    }

    Assemble() {
        var obj = [];
        for (var Row of this.Rows) {
            var Row = Row.Assemble();
            Row.OwnerReference = this.Owner.ObjectReferenceValueInitial//.MetaID// + "_" + this.Owner.ObjectReference.ObjectID;
            obj.push(Row);
        }
        return obj;
    }

}

class DynamicObjectViewer {

    set Value(value) {
        for (var Field of this.Fields) {
            if (value[Field.Property] != undefined) {
                this[Field.Property].Value = value[Field.Property];
            }
        }
        for (var TabularSection of this.TabularSections) {
            if (value[TabularSection] != undefined) {
                this[TabularSection].Value = value[TabularSection];
            }
        }
    }

    get Value() {
        var obj = {};
        for (var Field of this.Fields) {
            obj.ObjectData[Field.Property] = this[Field.Property].Value;
        }
        for (var TabularSection of this.TabularSections) {
            if (this[TabularSection] != undefined) {
                obj.TabularSections[TabularSection] = this[TabularSection].Value;
            }
        }
        return obj;
    }

    constructor(id, ObjectType, FieldsGivenByServer, BaseAddress, ObjectReferenceValueInitial, VUECLASS) {
        this.ObjectID = ObjectReferenceValueInitial.split('_')[1];
        this.ObjectType = ObjectType;
        this.RecordButton = document.createElement("button");
        //this.RecordButton.innerHTML = "Record";

        var RecordImage = document.getElementById("RecordImage").cloneNode(false);
        RecordImage.hidden = false;

        this.RecordButton.setAttribute("class", "NativeButton");

        this.RecordButton.appendChild(RecordImage);
        var obj = this;
        this.RecordButton.onclick = function () {
            obj.RecordObjectOnServer();
        }

        this.ObjectReferenceValueInitial = ObjectReferenceValueInitial;
        this.BaseAddress = BaseAddress;
        var FieldsGivenByServerParsed = JSON.parse(FieldsGivenByServer);
        this.Fields = FieldsGivenByServerParsed.Fields;
        this.TabularSections = FieldsGivenByServerParsed.TabularSections;
        var Fields = this.Fields
        this.WholeElement = document.createElement("div");
        this.Header = document.createElement("h2");
        this.WholeElement.prepend(this.RecordButton);
        this.WholeElement.id = id;
        this.FieldsTable = document.createElement("table");
        this.FieldsTable.id = "FieldsTable_" + id;
        this.FieldsTable.setAttribute("class", "table");
        this.FieldsTable.Body = document.createElement("tbody");
        var a = 0;
        for (var Field of Fields) {
            if (Field.Property == "ObjectReference" && a == 0) {
                a = 1;
                this[Field.Property] = new ValueContainer("", Field.Property, Field.Type, "", Field.ModelName, Boolean(Field.Editable),
                    this.BaseAddress, document.getElementById("MainHeader"), true, false);
            } else {
                if (Field.Showable) {
                    var NewRow = document.createElement("tr");
                    var NewColumn = document.createElement("td");
                    NewColumn.innerHTML = Field.Representation;
                    NewRow.appendChild(NewColumn);

                    NewColumn = document.createElement("td");
                    this[Field.Property] = new ValueContainer("", Field.Property, Field.Type, "", Field.ModelName, Boolean(Field.Editable),
                        this.BaseAddress, NewColumn);
                    NewRow.appendChild(NewColumn);
                    this.FieldsTable.Body.appendChild(NewRow);
                }
            }
        }

        this.FieldsTable.appendChild(this.FieldsTable.Body);
        this.WholeElement.appendChild(this.FieldsTable);
        var obj = this;
        for (var TabularSectionName of this.TabularSections) {
            GetMetadata(TabularSectionName, this.BaseAddress + "/Universal/RetrieveMetadata", "List",
                function (responseText) {
                    obj[TabularSectionName] = new DynamicTable(obj.ObjectID, responseText, obj.BaseAddress, TabularSectionName, "", obj.WholeElement, obj, { "OwnerID": obj.ObjectID }, true)
                    document.getElementById("Body").appendChild(obj[TabularSectionName].WholeElement);
                })
        }

        this.GetContents();
    }

    GatherFilters() {
        return {};
    }

    GetContents() {
        var Request = new XMLHttpRequest();
        var Address = this.BaseAddress + "/" + "universal" + "/" + "retrieve";
        Request.open("POST", Address, true);
        Request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
        var SentData = { 'ObjectReference': this.ObjectReferenceValueInitial }
        var json = JSON.stringify(SentData);
        Request.send(json);
        var obj = this;
        Request.onreadystatechange = function () {
            if (this.readyState == 4) {
                var ParsedJSON = JSON.parse(Request.responseText);
                obj.Value = ParsedJSON.ObjectData;
                if (obj.ObjectID == "0") { obj.ObjectID = ParsedJSON.ObjectReference.split('_')[1]; }
                //for (var TabSection of obj.TabularSections) {
                //    obj[TabSection].Value = ParsedJSON.TabularSections[TabSection];
                //}
            }
        }

    }

    Assemble() {
        var obj =
        {
            "ObjectType": this.ObjectType, "ObjectData": { 'ID': this.ObjectID }
        };
        for (var Field of this.Fields) {
            if (Field.Type != "Reference") {
                obj.ObjectData[Field.Property] = this[Field.Property].Value;
            }
            else {
                var val = this[Field.Property].Value;
                obj.ObjectData[Field.Property] = val.MetaID + "_" + val.ObjectID;
            }
        }

        obj.TabularSections = {};

        for (var TabularSectionName of this.TabularSections) {
            obj.TabularSections[TabularSectionName] = this[TabularSectionName].Assemble();
        }

        return obj;
    }

    RecordObjectOnServer() {
        var Request = new XMLHttpRequest();
        var Address = this.BaseAddress + "/" + "universal" + "/" + "record";
        Request.open("POST", Address, true);
        Request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
        var SentData = this.Assemble();
        var json = JSON.stringify(SentData);
        Request.send(json);
        var obj = this;
        Request.onreadystatechange = function () {
            if (this.readyState == 4) {
                var Response = JSON.parse(this.responseText);
                if (Response.Status != "Error") {
                    obj.Value = Response.ObjectData;
                    obj.Value = Response.TabularSections;
                }
            }
        }
    }

}
