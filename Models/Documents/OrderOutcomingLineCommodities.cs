﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;

namespace SoleTraderBookkeepingApp.Models
{
    [Metadata(52, "Commodities", "Commodities")]
    public class OrderOutcomingLineCommodities : TableLine
    {
        [ForeignKey("Commodity")]
        public ulong CommodityID { get; set; }

        [NotMapped]
        [Showable("Commodity", "Commodity", 1)]
        public Reference CommodityReference {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<Commodity>();
            }
            set
            {
                //var Ref = CommodityReference;
                //if ((Ref.MetaID * (int)Ref.ObjectID * value.MetaID * (int)value.ObjectID * (Ref.MetaID - value.MetaID)) > 0) { 
                CommodityID = value.ObjectID; 
            //}
            }
        }

        [NotMapped]
        public Reference OwnerReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<OrderOutcoming>();
            }
            set
            {
                if (OwnerReference.MetaID * (int)OwnerReference.ObjectID * value.MetaID * (int)value.ObjectID == 0
                  || OwnerReference.MetaID == value.MetaID) { OwnerID = value.ObjectID; }
            }
        }


        [Showable("Quantity", "", 2)]
        public float Quantity { get; set; }

        [Showable("Price", "", 2)]
        public float Price { get; set; }

        [ForeignKey("OrderOutcoming")]
        public override UInt64 OwnerID { get; set; }

        [NotMapped]
        [Showable("Sum","", 1100)]
        public float Sum { get { return Price * Quantity; } }

        [ForeignKey("MeasureUnit")]
        public ulong MeasureUnitID { get; set; }

        [NotMapped]
        [Showable("Measure unit", "MeasureUnit", 4)]
        public Reference MeasureUnitReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<MeasureUnit>();
            }
            set
            {
                if (MeasureUnitReference.MetaID * (int)MeasureUnitReference.ObjectID * value.MetaID * (int)value.ObjectID == 0
                  || MeasureUnitReference.MetaID == value.MetaID) { MeasureUnitID = value.ObjectID; }
            }
        }
    }
}
