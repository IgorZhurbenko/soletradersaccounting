﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SoleTraderBookkeepingApp.Models
{
    [Metadata(12, "Incoming order", "Incoming orders")]
    [Navigatable]
    [HasTabularSections(typeof(OrderIncomingLineCommodities))]
    public class OrderIncoming : Order
    {
        [ForeignKey("Counterparty")]
        [JsonIgnore]
        public ulong CustomerID { get; set; }

        [NotMapped]
        [Showable("Customer", "Counterparty", 1, true)]
        public Reference CustomerReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<Counterparty>();
            }
            set
            {
                CustomerID = value.ObjectID; 
            }
        }
    }
}
