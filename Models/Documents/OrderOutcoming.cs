﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;

namespace SoleTraderBookkeepingApp.Models
{
    [Navigatable]
    [Metadata(17,"Outcoming order", "Outcoming orders")]
    [HasTabularSections(typeof(OrderOutcomingLineCommodities))]
    public class OrderOutcoming: Order
    {
        [ForeignKey("Counterparty")]
        public ulong SupplierID { get; set; }

        [NotMapped]
        [Showable("Supplier", "Counterparty", 1, true)]
        public Reference SupplierReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<Counterparty>();
            }
            set
            {
                if (SupplierReference.MetaID * (int)SupplierReference.ObjectID * value.MetaID * (int)value.ObjectID == 0
                          || SupplierReference.MetaID == value.MetaID)
                { SupplierID = value.ObjectID; }
            }
        }

    }
}
