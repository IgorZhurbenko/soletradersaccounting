﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;

namespace SoleTraderBookkeepingApp.Models
{
    [Navigatable]
    [Metadata(11, "Commodity type", "Commodity types")]
    public class CommodityType: Informative
    {
                
    }
}
