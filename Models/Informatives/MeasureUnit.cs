﻿using SoleTraderBookkeepingApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{

    [Metadata(15,"Measure unit", "Measure units")]
    public class MeasureUnit: Informative
    {
        [ForeignKey("Measure")]
        public int MeasureID;

        [NotMapped]
        public Reference MeasureReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<CommodityType>();
            }
            set
            {
                if (MeasureReference.MetaID * (int)MeasureReference.ObjectID * value.MetaID * (int)value.ObjectID == 0
                  || MeasureReference.MetaID == value.MetaID) { MeasureID = (int)value.ObjectID; }
            }
        }

        public float Coefficient { get; set; }
    }
}
