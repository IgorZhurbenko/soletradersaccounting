﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    [Navigatable]
    [Metadata(18, "Employee", "Employees")]
    public class Employee : Informative
    {
        [ForeignKey("Position")]
        public ulong PositionID { get; set; }
        [NotMapped]
        [Showable("Position", "Position", 2)]
        public Reference PositionReference {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<Position>();
            }
            set
            {
                PositionID = value.ObjectID;
            }
        }
    }
}
