﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SoleTraderBookkeepingApp.Models;

namespace SoleTraderBookkeepingApp.Models
{
    //public enum CounterPartiesType
    //{
    //    Supplier,
    //    Customer
    //}
    [Navigatable]
    [Metadata(16, "Counterparty", "Counterparties")]
    public class Counterparty : Informative
    {
        [JsonIgnore]
        [ForeignKey("CounterpartyType")]
        public ulong TypeID { get; set; }
        
        [Showable("Type", "CounterpartyType", 2, true)]
        [NotMapped]
        public Reference TypeReference {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<CounterpartyType>();
            }
            set
            {
                if (TypeReference.MetaID * (int)TypeReference.ObjectID * value.MetaID * (int)value.ObjectID == 0
                  || TypeReference.MetaID == value.MetaID) { TypeID = value.ObjectID; }
            }
        }
    }
}
