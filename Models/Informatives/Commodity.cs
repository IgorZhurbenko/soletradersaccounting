﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoleTraderBookkeepingApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using System.Composition;
using SoleTraderBookkeepingApp.Data;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SoleTraderBookkeepingApp.Models
{
    [Metadata(10, "Commodity", "Commodities")]
    [Navigatable]
    public class Commodity: Informative
    {
        [ForeignKey("CommodityType")]
        [JsonIgnore]
        public UInt64 TypeID { get; set; }

        [Showable("Commodity type", "CommodityType", 2, true)]
        [NotMapped]
        public Reference TypeReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<CommodityType>();
            }
            set { if (TypeReference.MetaID * (int)TypeReference.ObjectID * value.MetaID * (int)value.ObjectID == 0 
                    || TypeReference.MetaID == value.MetaID) { TypeID = value.ObjectID; } }
        }

        [Showable("Name", "", 2, false)]
        public override string Name { get; set; }
                 
    }
}
