﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    
    [Metadata(14, "Counterparty type", "Counterparty types")]
    public class CounterpartyType : Informative
    {
    }
}
