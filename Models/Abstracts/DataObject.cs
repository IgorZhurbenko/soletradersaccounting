﻿using Microsoft.EntityFrameworkCore;
using SoleTraderBookkeepingApp.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    public class DataObject
    {
        public Reference GetOutgoingReference<ReferenceSourceType>()
        {
            Type SourceType = typeof(ReferenceSourceType);

            var ReferenceSourceID = this.GetType().GetProperties().Where(prop => prop.GetCustomAttribute<ForeignKeyAttribute>() != null
                    && prop.GetCustomAttribute<ForeignKeyAttribute>().Name == SourceType.Name).First().GetValue(this);

            return new Reference(SourceType.GetCustomAttribute<MetadataAttribute>().MetaID, (ulong)ReferenceSourceID, SourceType.GetCustomAttribute<MetadataAttribute>().MetaID.ToString() + "_" + ReferenceSourceID.ToString());
        }

        public Reference GetOutgoingReference(string ReferenceSourceName, string PropertyName)
        {
            Type SourceType = Type.GetType(ReferenceSourceName);
            return new Reference(
                SourceType.GetCustomAttribute<MetadataAttribute>().MetaID,
                (ulong)this.GetType().GetProperty(PropertyName).GetValue(this),
                SourceType.GetCustomAttribute<MetadataAttribute>().MetaID.ToString()
                + "_" + this.GetType().GetProperty(PropertyName).GetValue(this).ToString()
                );
        }

        public Reference GetOutgoingReference(string ReferenceSourceName)
        {
            Type SourceType = Type.GetType(this.GetType().Namespace + "." + ReferenceSourceName);
            PropertyInfo ResponsibleProperty = this.GetType().GetProperties().Where(
                prop => (prop.GetCustomAttribute(typeof(ForeignKeyAttribute)) as ForeignKeyAttribute).Name == ReferenceSourceName
                ).First();
            return new Reference(
                SourceType.GetCustomAttribute<MetadataAttribute>().MetaID,
                (ulong)ResponsibleProperty.GetValue(this),
                SourceType.GetCustomAttribute<MetadataAttribute>().MetaID.ToString()
                + "_" + ResponsibleProperty.GetValue(this).ToString()
                );
        }

        public Reference GetOutgoingReferenceWithRepresentation(string ReferenceSourceName, string PropertyName)
        {
            Type SourceType = Type.GetType(this.GetType().Namespace + "." + ReferenceSourceName);
            Reference res;
            using (var dbcontext = new FinancialDataContext())
            {
                DbSet<Entity> Set = (DbSet<Entity>)typeof(FinancialDataContext).GetProperties().Where(prop => prop.PropertyType == typeof(DbSet<>)
                && prop.PropertyType.GenericTypeArguments[0] == SourceType).First().GetValue(dbcontext);
                res = Set.Find((ulong)this.GetType().GetProperty(PropertyName).GetValue(this)).ObjectReference;
            }
            return res;
        }

        public Reference GetOutgoingReferenceWithRepresentation(string ReferenceSourceName)
        {
            Type SourceType = Type.GetType(this.GetType().Namespace + "." + ReferenceSourceName);

            var ReferenceSourceID = this.GetType().GetProperties().Where(prop => prop.GetCustomAttribute<ForeignKeyAttribute>() != null
                    && prop.GetCustomAttribute<ForeignKeyAttribute>().Name == ReferenceSourceName).First().GetValue(this);

            Reference res;

            using (var dbcontext = new FinancialDataContext())
            {
                Entity obj = dbcontext.Find(SourceType, ReferenceSourceID) as Entity;
                res = obj.ObjectReference;
            }
            return res;
        }

        public Reference GetOutgoingReferenceWithRepresentation<ReferenceSourceType>()
        {
            Type SourceType = typeof(ReferenceSourceType);

            var ReferenceSourceID = this.GetType().GetProperties().Where(prop => prop.GetCustomAttribute<ForeignKeyAttribute>() != null
                    && prop.GetCustomAttribute<ForeignKeyAttribute>().Name == SourceType.Name).First().GetValue(this);

            Reference res;

            using (var dbcontext = new FinancialDataContext())
            {
                Entity obj = dbcontext.Find(SourceType, ReferenceSourceID) as Entity;
                res = obj != null ? obj.ObjectReference : new Reference(0, 0, "0_0");
            }
            return res;
        }

    }
}
