﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    public class Informative: Entity
    {
        public override string GetReferenceRepresentation()
        {
            return this.Name;
        }

        [Showable("Name", "", 2, false)]
        public virtual string Name { get; set; }
    }
}
