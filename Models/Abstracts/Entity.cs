﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using SoleTraderBookkeepingApp.Data;

using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using SoleTraderBookkeepingApp.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Net.WebSockets;
using SoleTraderBookkeepingApp.Controllers;
using System.Runtime.InteropServices.WindowsRuntime;

namespace SoleTraderBookkeepingApp.Models
{
    public abstract class Entity : DataObject
    {
        [Key]
        public virtual UInt64 ID { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public int DateINT{ set { this.Date = value; } get { return this.Date; } }

        //[Showable("Date", "")]
        [Column(TypeName = "integer")]
        [NotMapped]
        public DateString Date
        {
            get; set;
        } = 0;

        //[Showable("Deleted")]
        public bool MarkedForDeletion { get; set; }

        public virtual string GetReferenceRepresentation()
        {
            return $"Entity number {this.ID} of Date {this.Date}";
        }

        public virtual bool Record(FinancialDataContext Context)
        {
            try
            {
                if (this.ID != 0)
                {
                    var SetProperty = Context.Update(this);
                    Context.SaveChanges();
                    return true;
                }
                else
                {
                    Context.Add(this);
                    Context.SaveChanges();
                    return true;
                }
            }
            catch { return false; }
        }    

        private protected Reference FormReference()
        {
            return new Reference(this.GetType().GetCustomAttribute<MetadataAttribute>().MetaID, this.ID, this.GetReferenceRepresentation()) { ModelName = this.GetType().Name };
        }

        [NotMapped]
        [Showable("Entry", "", 0)]
        [NotFilterable]
        public virtual Reference ObjectReference
        {            
            get
            {
                if (this != null)
                {
                    return this.FormReference();
                }
                else
                { return new Reference(0, 0, "Empty"); }
            }
        
        }

        
        public object[] GetTabularSectionMembers(string TabularSectionName)
        {
            Type TabularSectionType = Type.GetType(this.GetType().Namespace.Split('.')[0] + ".Models." + TabularSectionName);

            Type ThisType = this.GetType();

            Dictionary<PropertyInfo, string> Filter = new Dictionary<PropertyInfo, string>();

            PropertyInfo NeededPropertyOfLine = TabularSectionType.GetProperties()
                .Where(prop => prop.GetCustomAttribute<ForeignKeyAttribute>().Name == ThisType.Name).First();

            Filter.Add(NeededPropertyOfLine, this.ID.ToString());

            return (typeof(FinancialDataContext).GetProperties()
           .Where(
               prop => (prop.PropertyType.GenericTypeArguments.Count() > 0)
               && (prop.PropertyType.GenericTypeArguments.First() == TabularSectionType))
           .First()
           .GetValue(new FinancialDataContext()) as DbSet<object>)
           .Where(obj => obj.FitsFilterAsString(Filter)
           ).ToArray<object>();

        }

        public string[] GetTabularSectionsNames()
        {
            var TSAttr = this.GetType().GetCustomAttribute<HasTabularSectionsAttribute>();
            if (TSAttr != null)
                return TSAttr.AttachedTabularSectionsLinesTypesNames;
            else { return new string[0]; }
        }

        public Entity() { }

        
    }
}
