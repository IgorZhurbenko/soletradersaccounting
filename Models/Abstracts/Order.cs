﻿
using SoleTraderBookkeepingApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using System.Threading.Tasks;


namespace SoleTraderBookkeepingApp.Models
{
    public abstract class Order : Document
    {
        //public int IssueDate { get; set; }
        [Showable("Expected settlement date", "")]
        [Column(TypeName = "integer")]
        [NotMapped]
        public DateString ExpectedSettlementDate { get; set; }

        [JsonIgnore]
        public int ExpectedSettlementDateINT { get { return ExpectedSettlementDate; } set { ExpectedSettlementDate = value; } }

        [Column(TypeName = "integer")]
        [JsonIgnore]
        public int SettlementDateINT { get { return SettlementDate; } set { SettlementDate = value; } }

        [NotMapped]
        [Showable("Actual settlement date", "")]
        public DateString SettlementDate { get; set; } = 0;

        public override string GetReferenceRepresentation()
        {

            return this.GetType().GetCustomAttribute<MetadataAttribute>().SingleName +
                ((this.ExpectedSettlementDateINT > this.SettlementDateINT) ? " for " + this.ExpectedSettlementDate
                :
                " of " + this.SettlementDate);
        }

        [ForeignKey("Employee")]
        public UInt64 Responsible { get; set; }

        [NotMapped]
        [Showable("Responsible", "Employee")]
        public Reference ResponsibleReference
        {
            get
            {
                return this.GetOutgoingReferenceWithRepresentation<Employee>();
            }
            set
            {
                Responsible = value.ObjectID;
            }
        }
    }
}

