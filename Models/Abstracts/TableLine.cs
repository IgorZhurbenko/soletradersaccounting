﻿using Microsoft.EntityFrameworkCore;
using SoleTraderBookkeepingApp.Data;
using SoleTraderBookkeepingApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    public class TableLine : DataObject
    {
        public UInt64 ID { get; set; } = 0;
        public virtual UInt64 OwnerID { get; set; }

        
        public static TableLine[] RecordLinesArray(FinancialDataContext context, Type LineType, ulong OwnerID, IEnumerable<TableLine> Lines)
        {
            //Type.GetType("DbSet").MakeGenericType(LineType) = 
            var GettingProperty = typeof(FinancialDataContext).GetProperties().Where(prop => prop.PropertyType.IsGenericType
           && prop.PropertyType.GetGenericArguments().First() == LineType).First();

            var FormerLines = (GettingProperty.GetValue(context) as IEnumerable<TableLine>)
           .Where(Line => Line.OwnerID == OwnerID);

            context.RemoveRange(FormerLines);
            context.SaveChanges();

            context.DisposeAsync();

            context = new FinancialDataContext();

            context.AddRange(Lines);

            try 
            { 
                context.SaveChanges(); 
                return (GettingProperty.GetValue(context) as IEnumerable<TableLine>)
                    .Where(Line => Line.OwnerID == OwnerID).ToArray(); 
            }
            catch 
            {                
                context.AddRange(FormerLines);
                context.SaveChanges();
                return null;
            }

        }

    }
}
