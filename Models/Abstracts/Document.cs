﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp.Models
{
    public abstract class Document: Entity
    {
        [Showable("Number", "", 1)]
        public ulong Number { get; set; } = 0;

        public bool Moved { get; set; }

        public virtual void Move()
        {

        }

        public override string GetReferenceRepresentation()
        {
            return this.GetType().GetCustomAttribute<MetadataAttribute>().SingleName + " №" + this.Number.ToString() + " of " + this.Date.Representation;
        }
    }

    
}
