﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoleTraderBookkeepingApp
{
    public enum ComparisonTypes 
    { 
        Equal, 
        Less, 
        Greater, 
        LessOrEqual, 
        GreaterOrEqual, 
        Among, 
        NotAmong, 
        ContainsIgnoringCase, 
        ContainsConsideringCase
    }
    public struct Criterion
    {
        public string PropertyName { get; set; }
        public string ValueTypeName { get; set; }
        public string Value { get; set; }
        public string ComparisonTypeString { 
            set 
            {
                this.ComparisonType = (ComparisonTypes)Enum.Parse(typeof(ComparisonTypes), "value");
            }
        }

        public ComparisonTypes ComparisonType { get; set; }
    }
    public struct UniversalQueryJSONStructure
    {
        public string ObjectName;
        public Criterion[] Criteria;
    }

}
